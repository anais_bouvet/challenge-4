# Challenge 4

Description of the challenge 4 week (1 June to 4th).

Week from June 1 to 4th 2021. Challenge n ° 4 of the MDEF Master. I worked with Bothaina to continue our project around cosmetics. We chose to continue working on our sheet masks for this last challenge.

<html>

<!-- !PAGE CONTENT! -->
<div class="w3-content" style="max-width:1500px">

  <!-- Header -->
  <div class="w3-row">
  <div class="w3-half w3-container">
    <h1 class="w3-xxlarge w3-text-light-grey">June 2021</h1>
    <h1 class="w3-xxlarge w3-text-grey">Week 1/4th of June </h1>
    <h1 class="w3-jumbo">Challenge 4</h1>
    <h1 class="w3-xxlarge w3-text-light-grey">We have been created a Website for our project and did others materials experiments for the mold.</h1>
    <h1 class="w3-xxlarge w3-text-grey"> </h1>
    <h1 class="w3-jumbo">Sheet masks</h1>
  </div>
  <div class="w3-half w3-container w3-xxlarge w3-text-grey">
      <p class=""> </p>
  </div>
  </div>

  <!-- Photo Grid -->

  <h1 class="w3-jumbo">Website</h1>

<p class="w3-xlarge">
We wanted to create our website
</p>

<p class="w3-xlarge">
1.Design the website with the aesthetic we wanted.
</p>

<p class="w3-xlarge">
2.Access some option for free like the instagram gallery that will be visible from our instagram and maybe a shop option.
</p>

<p class="w3-xlarge">
3.Create a website for after MDEF program for our project.
</p>

<p class="w3-xlarge">
This challenge we focused on finding out the best recipe for our molds and cast more molds for our project event and start with our website for the brand.

We started with the website :
We wanted to create a simple website showing
-our goal with this project
-some recipes that we want to do
-a place where people can share there recipes
-inspirational Instagram feed  

We started by the website and decide for a visual for our project. We decided to design our website with a minimal aesthetic, by using NICEPAGE. We couldn't get the aesthetic we wanted and it was quite hard to understand the logic of it.
</p>

![](Images/test_website.JPG)

<p class="w3-xlarge">
After watching Wix vs Squarespace (Best Website Builder 2021) video (1) we decided to change and try square space. To have something easy to understand and ready at the end of the week.
</p>

![](Images/About_us.JPG)

<p class="w3-xlarge">

The creation was not that easy because everything works in block on square space so we couldn't personalized 100% our website that was frustated, for example the font. We can add our font on you space but then you have to use CSS customize service of square space.


</p>

![](Images/customize_font.JPG)

![](Images/edit_part.JPG)

  <h1 class="w3-jumbo">Recipes and tests</h1>

<p class="w3-xlarge">
Recipe 1 :
We bought some bees wax to try to make a mold with a material that will stay in the loop. For instance if you have a mold at home and you break it we wanted to be able to re-melt it and give it back to you.
We started with a ratio of 50/50.
</p>

<p class="w3-xlarge">
- 100 gr of Bees wax
</p>

<p class="w3-xlarge">
- 100 gr of pin resin
</p>
</p>

![](Images/Pin_resin.JPG)

![](Images/Mold.JPG)

![](Images/Moldinbeeswax.JPG)

<p class="w3-xlarge">
The result is not good because the mold is melting with the temperature. We will have to continue our research for the material itself of the mold and the quantity and stay in our values.
</p>

![](Images/result.JPG)

<p class="w3-xlarge">
Bibliography :
</p>

<p class="w3-large ">
  <a href="https://www.youtube.com/watch?v=-OkLNYbpNfU">(1) Wix vs Squarespace (Best Website Builder 2021).
  </a>
</p>

<p class="w3-xlarge">
To follow the steps of this project in the precedent challenges :
</p>

<p class="w3-large ">
  <a href="https://gitlab.com/anais_bouvet/challenge_1_mdef_sheet_masks"> Link Challenge 1.
  </a>
</p>
<p class="w3-large ">
  <a href="https://gitlab.com/anais_bouvet/challenge-2-mdef"> Link Challenge 2.
  </a>
</p>
<p class="w3-large ">
  <a href="https://gitlab.com/Bothaina_Alamri/challenge-3-mdef"> Link Challenge 3.
  </a>
</p>

<!-- End Page Content -->
</div>


</body>
</html>
